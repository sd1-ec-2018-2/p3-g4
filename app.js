var express = require('express');            
var app = express();	                   
var bodyParser = require('body-parser');    
var cookieParser = require('cookie-parser'); 
var path = require('path');                 
var amqp = require('amqplib/callback_api');  

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var id_gen = 0; 
var users = {}; 
var amqp_conn;
var amqp_canal;

amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_canal = ch;
	});
});

app.post('/login', function (req, res) { 	
	res.cookie('nick', req.body.nome);
	if(req.body.canal && req.body.canal[0]!='#'){
		req.body.canal = '#'+req.body.canal;
	}
	res.cookie('canal', req.body.canal);
	res.cookie('servidor', req.body.servidor);
	res.redirect('/');
});

function sendToserver (comando, msg) {
	
	msg = new Buffer(JSON.stringify(msg));
	
	amqp_canal.assertQueue(comando, {durable: false});
	amqp_canal.sendToQueue(comando, msg);
	
}

function receiveFromserver (id, callback) {
	
	amqp_canal.assertQueue("user_"+id, {durable: false});
	
	amqp_canal.consume("user_"+id, function(msg) {
		
		callback(id, JSON.parse(msg.content.toString()));
		
	}, {noAck: true});

	amqp_canal.assertQueue("ping_"+id, {durable: false});
	amqp_canal.consume("ping_"+id, function(message){
		var msg = message.content.toString();
		users[id].cache.push({"timestamp": Date.now(), 
	   "nick": "IRC Server", "msg": "pong: " + msg});
	}, {noAck:true});

	amqp_canal.assertQueue("whois_"+id, {durable: false});
	amqp_canal.consume("whois_"+id, function(message){
		var msg = message.content.toString();
		users[id].cache.push({"timestamp": Date.now(), 
	   "nick": "IRC Server", "msg": "->: " + msg});
	}, {noAck:true});
}


app.get('/', function (req, res) {
	if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
		id_gen++; 
		var id = id_gen;
		users[id] = {cache: [{
			"timestamp": Date.now(), 
	  		 "nick": "IRC Server",
	  		 "msg": "Bem vindo ao servidor IRC"}]}; 
	
	   res.cookie('id', id);
	   
	   var msg = { "id": id, 	"servidor": req.cookies.servidor, "nick": req.cookies.nick, 
		"canal": req.cookies.canal };
	
	   users[id].id       = msg.id;
	   users[id].servidor = msg.servidor;
	   users[id].nick     = msg.nick;
	   users[id].canal    = msg.canal;
	   
	   sendToserver("registro_conexao", msg);
	   receiveBroad(function(msg){
		   var i =0;
			for(i = 0; i < 1000; i++){
				if(users[i]){
					users[i].cache.push(msg);
				}
			}
	   });

	   receiveFromserver(id, function (id_real, msg) {

		   if(msg.nick != "IRC Server"){
		  	 users[id_real].nick = msg.nick;
		   }
		   if(msg.canal)users[id_real].canal = msg.canal;

		   users[id_real].cache.push(msg);
	   });
	   res.sendFile(path.join(__dirname, '/index.html'));
	}
	else {
		res.sendFile(path.join(__dirname, '/login.html'));
	}
});

app.get('/obter_mensagem/:timestamp', function (req, res) {
	var id = req.cookies.id;
	res.cookie('nick', users[id].nick);
	var response = users[id].cache;
	users[id].cache = [];
	
	res.append('Content-type', 'application/json');
	res.send(response);
});

app.post('/gravar_mensagem', function (req, res) {

	users[req.cookies.id].cache.push(req.body);
	sendToserver("gravar_mensagem", {
		"id": req.cookies.id,
		"nick": users[req.cookies.id].nick,
		"canal": users[req.cookies.id].canal, 
		"msg": req.body.msg
	});

	res.end();
});

function sendToserver (fila, msg) {
	msg = new Buffer(JSON.stringify(msg));
	amqp_canal.assertQueue(fila, {durable: false});
	amqp_canal.sendToQueue(fila, msg);
}

function receiveBroad(callback){
	   var ex = 'logs';

    amqp_canal.assertExchange(ex, 'fanout', {durable: false});

    amqp_canal.assertQueue('', {exclusive: true}, function(err, q) {
      amqp_canal.bindQueue(q.queue, ex, '');

      amqp_canal.consume(q.queue, function(msg) {
		callback(JSON.parse(msg.content.toString()));
      }, {noAck: true});
    });
}

app.listen(2000, function () {
	console.log('Servidor rodando no endereco: http://localhost:2000/');	
});
